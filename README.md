# InfoSec Mini-Project - Server, Client apps for information exchange


The purpose of this project is to build an Android app which will connect to a local FTP server using the configuration details provided by the user to share files from the android client to the ftp server over WiFi while also optimizing battery performance. For LAN Chat, A client app with option to open a socket with the configuration given by the user is developed and the server app will accept the connection and messages which are sent by multiple client instances are received by the server app.

---

For short text content, feature to exchange simple text messages between devices in the same LAN network is provided (eg: message a Microsoft Teams link received through WhatsApp to a PC in the same LAN and between two mobile devices (using sockets))

## Steps to Run
1. Open server, client applications in android studio / download apks from release folder

2. Start the server for specific port number in server app

3. Connect to server from client app by specifying the ip address and port number

4. Begin transfer of information between server and client
