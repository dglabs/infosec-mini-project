package com.dglabs.jsocket;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.text.format.Formatter;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import static com.dglabs.jsocket.TcpServerService.SERVICEPORT;
@SuppressLint("SetTextI18n")
public class MainActivity extends AppCompatActivity implements View.OnClickListener,
        onServiceStartListener {
    static Context context;
    TextView mStartStop,mStatus, mIpAddress, mClientData;
    private boolean isServiceStopped =true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        context = this;
        Toolbar myToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);
        mStartStop = findViewById(R.id.startstop);
        mStatus = findViewById(R.id.status);
        mIpAddress = findViewById(R.id.ipaddress);
        mClientData = findViewById(R.id.clientdata);
        mStartStop.setOnClickListener(this);

        mStatus.setText("Welcome");
        WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(WIFI_SERVICE);
        String ipAddress = Formatter.formatIpAddress(wifiManager.getConnectionInfo().getIpAddress());
        Log.d("Dhrisha:", "IP Address: " + ipAddress);
        mIpAddress.setText("Server IP Address: " + ipAddress + ":" + SERVICEPORT);
        mClientData.setText("Data Received: null" );
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.startstop:
                startStopService();
                break;
        }
    }
    private void startStopService() {
        if(isServiceStopped) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                startForegroundService(new Intent(this, TcpServerService.class));
            } else {
                startService(new Intent(this, TcpServerService.class));
            }
            isServiceStopped = false;
            mStartStop.setText("Stop");
        }
//        else {
//            isServiceStopped = true;
//            stopService(new Intent(this, TcpServerService.class));
//            mStartStop.setText("Start");
//        }
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        isServiceStopped = savedInstanceState.getBoolean("SERVICESTATE");
        if(isServiceStopped)
            mStartStop.setText("Start");
        else
            mStartStop.setText("Stop");
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean("SERVICESTATE",isServiceStopped);
    }

    @Override
    public void onServiceStart() {
        mStatus.setText("Tcp Server is running in background at " + SERVICEPORT);
    }
    @Override
    public void onClientAccept(String message) {
        mStatus.setText(message);
    }

    @Override
    public void onClientDataReceived(String message) {

        if(message ==null)
            message = "null";
        mClientData.setText(mClientData.getText().toString()+ "\n" + message);

    }
}