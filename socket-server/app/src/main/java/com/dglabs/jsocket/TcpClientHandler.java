package com.dglabs.jsocket;
import android.content.Context;
import android.util.Log;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class TcpClientHandler extends Thread{
    DataInputStream dataInputStream;
    private DataOutputStream dataOutputStream;
    private String TAG = getClass().getSimpleName();
    private Context context;
    public TcpClientHandler(Context activityContext,DataInputStream dataInputStream, DataOutputStream dataOutputStream) {
        this.dataInputStream = dataInputStream;
        this.dataOutputStream = dataOutputStream;
        context = activityContext;

        while (true) {
            try {
                if(dataInputStream.available() > 0){
                    callDataReceived(dataInputStream.readUTF());
                    dataOutputStream.write("hello".getBytes());
                    dataOutputStream.flush();
                    sleep(2000L);
                }
                callDataReceived("Data output Stream not available");

            } catch (IOException | InterruptedException e) {
                e.printStackTrace();
                try {
                    dataInputStream.close();
                    dataOutputStream.close();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        }

    }
    @Override
    public void run() {

    }
    private void callDataReceived(String s) {
        if(MainActivity.context instanceof onServiceStartListener)
            ((onServiceStartListener)(MainActivity.context)).onClientDataReceived(s);
        else
            Log.d("Dhrisha: ClientHandler","Context not an instance of OnServiceStart Listener");
    }
}