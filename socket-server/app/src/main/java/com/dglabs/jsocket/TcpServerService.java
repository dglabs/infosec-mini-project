package com.dglabs.jsocket;
import android.app.Activity;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.IBinder;
import android.text.format.Formatter;
import android.util.Log;
import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.atomic.AtomicBoolean;
import static java.lang.Thread.sleep;
interface onServiceStartListener {
    void onServiceStart();
    void onClientAccept(String message);
    void onClientDataReceived(String message);
}
public class TcpServerService extends Service {
    static final int SERVICEPORT = 7878, STATUS_MESSAGE = 100, CLIENT_DATA = 200  ;
    ServerSocket serverSocket;
    Context activityContext;
    private AtomicBoolean working = new AtomicBoolean(true);
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
    @Override
    public void onCreate() {
        activityContext = MainActivity.context;
        startMeForeground();
        Log.d("Dhrisha:" , "Starting Server");
        updateMain("Server socket created", STATUS_MESSAGE);
        new Thread(runnable).start();
    }
    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            Socket socket = null;
            byte[] buf;
            try {
                serverSocket = new ServerSocket(SERVICEPORT);
                while(working.get()){
                    if(serverSocket!=null){
                        socket = serverSocket.accept();
                        String clientIp = socket.getInetAddress().toString();
                        updateMain("client " + clientIp , STATUS_MESSAGE );
                        DataInputStream dataInputStream = new DataInputStream(socket.getInputStream());
//                        updateMain("Available bytes:" + socket.getInputStream().available() );
                        //updateMain("Available bytes:" + dataInputStream.available() );
                        //updateMain("Available bytes:" + socket.getInputStream().available());
                        DataOutputStream dataOutputStream = new DataOutputStream(socket.getOutputStream());
                        dataOutputStream.write("ack".getBytes());
                        buf = new byte[dataInputStream.available()];
                        dataInputStream.read(buf);
                        updateMain("read " + new String(buf), CLIENT_DATA );
                        //Thread thread = new TcpClientHandler(activityContext,dataInputStream,dataOutputStream);
                        //thread.start();
                        sleep(100L);
                    }
                    else
                        updateMain("Couldnot create server Socket", STATUS_MESSAGE);
                }
            } catch (IOException | InterruptedException e) {
                updateMain("io exception", STATUS_MESSAGE);
                e.printStackTrace();
                if(socket!=null) {
                    try {
                        socket.close();
                    } catch (IOException ioException) {
                        ioException.printStackTrace();
                    }
                }
            }
        }
    };
    private void updateMain(String message, int messageType) {
        Context activityContext = MainActivity.context;
        if(activityContext instanceof onServiceStartListener)
            ((Activity)activityContext).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if(messageType == STATUS_MESSAGE)
                        ((onServiceStartListener) activityContext).onClientAccept(message);
                    if(messageType == CLIENT_DATA)
                        ((onServiceStartListener) activityContext).onClientDataReceived(message);
                }
            });
        else
            Log.d("Dhrisha:","Context not an instance of OnServiceStart Listener");
    }
    @Override
    public void onDestroy() {
        working.set(false);
        super.onDestroy();
    }
    private void startMeForeground(){
        String NOTIFICATION_CHANNEL_ID = getPackageName();
        String channelName = "JSocket Server Background Service";
        Notification notification = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID)
                .setOngoing(true)
                .setSmallIcon(R.drawable.ic_launcher_foreground)
                .setContentTitle("Tcp Server is running in background at " + SERVICEPORT)
                .setPriority(NotificationManager.IMPORTANCE_MIN)
                .setCategory(Notification.CATEGORY_SERVICE)
                .build();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel chan = new  NotificationChannel(NOTIFICATION_CHANNEL_ID, channelName, NotificationManager.IMPORTANCE_NONE);
            chan.setLightColor(Color.BLUE);
            chan.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
            NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            manager.createNotificationChannel(chan);
            Log.d("Dhrisha:","Version is more or = O");
            startForeground(2, notification);
        } else {
            Log.d("Dhrisha:","Version is less than O");
            startForeground(1, notification);
        }
        Context activityContext = MainActivity.context;
        if(activityContext instanceof onServiceStartListener)
            ((onServiceStartListener)activityContext).onServiceStart();
        else
            Log.d("Dhrisha:","Context not an instance of OnServiceStart Listener");
    }
}